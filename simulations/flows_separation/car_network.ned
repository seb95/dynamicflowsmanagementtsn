//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package dynamicflowsmanagementtsn.simulations.flows_separation;

import ned.DatarateChannel;
import dynamicflowsmanagementtsn.tsn.node.ethernet.EthernetHost;
import nesting.node.ethernet.VlanEtherSwitchPreemptable;
import nesting.node.ethernet.VlanEtherHostQ;
import dynamicflowsmanagementtsn.tsn.node.ethernet.VlanEtherSwitchPreemptableReconfigurable;


network car_network
{
    @display("bgb=808,590");
    types:
        channel C_roots extends DatarateChannel
        {
            delay = 0.1us;
            datarate = 1Gbps;
        }
        channel C_branches extends DatarateChannel
        {
            delay = 0.1us;
            datarate = 1Gbps;
        }
        channel C_leaves extends DatarateChannel
        {
            delay = 0.1us;
            datarate = 1Gbps;
        }

    submodules:
        switchA: VlanEtherSwitchPreemptableReconfigurable {
            parameters:
                @display("p=301.75198,263.736");
            gates:
                ethg[6];
        }
        switchB: VlanEtherSwitchPreemptableReconfigurable {
            parameters:
                @display("p=451,264");
            gates:
                ethg[6];
        }
        ZC_FR: VlanEtherSwitchPreemptableReconfigurable {
            parameters:
                @display("p=106,147");
            gates:
                ethg[4];
        }
        ZC_RR: VlanEtherSwitchPreemptableReconfigurable {
            parameters:
                @display("p=613,147");
            gates:
                ethg[3];
        }
        ZC_FL: VlanEtherSwitchPreemptableReconfigurable {
            parameters:
                @display("p=106,435");
            gates:
                ethg[3];
        }
        ZC_RL: VlanEtherSwitchPreemptableReconfigurable {
            parameters:
                @display("p=613,435");
            gates:
                ethg[2];
        }
        telematics: EthernetHost {
            @display("p=311.25598,370.656");
        }
        HU: EthernetHost {
            @display("p=225.72,378.972");
        }
        diagnostic: EthernetHost {
            @display("p=427,347");
        }
        DA_CAM: EthernetHost {
            @display("p=137.808,238.788");
        }
        RSE: EthernetHost {
            @display("p=490,127");
        }
        AVR: EthernetHost {
            @display("p=388,183");
        }
        cam1: EthernetHost {
            @display("p=42,265");
        }
        cam2: EthernetHost {
            @display("p=239,468");
        }
        cam3: EthernetHost {
            @display("p=239,100");
        }
        cam4: EthernetHost {
            @display("p=676,265");
        }
        CAN_RL: EthernetHost {
            @display("p=613,497");
        }
        CAN_RR: EthernetHost {
            @display("p=613,80");
        }
        CAN_FR: EthernetHost {
            @display("p=106,80");
        }
        CAN_FL: EthernetHost {
            @display("p=106,497");
        }
    connections:
        cam2.ethg <--> C_leaves <--> ZC_FL.ethg[1];
        CAN_FL.ethg <--> C_leaves <--> ZC_FL.ethg[2];
        cam1.ethg <--> C_leaves <--> ZC_FR.ethg[1];
        cam3.ethg <--> C_leaves <--> ZC_FR.ethg[2];
        CAN_FR.ethg <--> C_leaves <--> ZC_FR.ethg[3];

        ZC_FL.ethg[0] <--> C_branches <--> switchA.ethg[1];
        ZC_FR.ethg[0] <--> C_branches <--> switchA.ethg[2];
        telematics.ethg <--> C_branches <--> switchA.ethg[3];
        HU.ethg <--> C_branches <--> switchA.ethg[4];
        DA_CAM.ethg <--> C_branches <--> switchA.ethg[5];

        switchA.ethg[0] <--> C_roots <--> switchB.ethg[0];

        ZC_RL.ethg[0] <--> C_branches <--> switchB.ethg[1];
        ZC_RR.ethg[0] <--> C_branches <--> switchB.ethg[2];
        diagnostic.ethg <--> C_branches <--> switchB.ethg[3];
        RSE.ethg <--> C_branches <--> switchB.ethg[4];
        AVR.ethg <--> C_branches <--> switchB.ethg[5];

        cam4.ethg <--> C_leaves <--> ZC_RR.ethg[1];
        CAN_RR.ethg <--> C_leaves <--> ZC_RR.ethg[2];
        CAN_RL.ethg <--> C_leaves <--> ZC_RL.ethg[1];
}
