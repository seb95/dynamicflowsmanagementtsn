//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TSN_IEEE8021Q_QUEUE_GATING_GATECONTROLLERMULTISCHEDULE_H_
#define TSN_IEEE8021Q_QUEUE_GATING_GATECONTROLLERMULTISCHEDULE_H_

#include "nesting/ieee8021q/queue/gating/GateController.h"
#include "Schedules.h"
#include <vector>

using namespace nesting;

namespace tsn {

class GateControllerMultischedule : public GateController{
protected:
    std::vector<Schedules*> schedules;

    virtual void initialize(int stage) override;
    virtual void check_schedule();

public:
    virtual void tick(IClock *clock, short kind) override;
};

}

#endif /* TSN_IEEE8021Q_QUEUE_GATING_GATECONTROLLERMULTISCHEDULE_H_ */
