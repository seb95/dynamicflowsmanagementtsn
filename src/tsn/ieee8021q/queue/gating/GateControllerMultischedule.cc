//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "GateControllerMultischedule.h"

namespace tsn{

Define_Module(GateControllerMultischedule);

void GateControllerMultischedule::initialize(int stage){
    GateController::initialize(stage);

    cModule* schedulesVectorModule = getSubmodule("schedulesModule",0);
    if(schedulesVectorModule!=nullptr){
        cModule::SubmoduleIterator it = cModule::SubmoduleIterator(schedulesVectorModule->getParentModule());
        for (; !it.end(); it++) {
            cModule* subModule = *it;
            if (subModule->isName(schedulesVectorModule->getName())) {
                Schedules* schedule = check_and_cast<Schedules*>(subModule);
                schedules.push_back(schedule);
            }
        }
    }
}

void GateControllerMultischedule::check_schedule(){
    if(!schedules.empty()){
        if(simTime().dbl() * 1000 >=  schedules.front()->par("scheduleTime").intValue() ){
            cXMLElement* xml = schedules.front()->par("scheduleXml").xmlValue();
            loadScheduleOrDefault(xml);
            schedules.erase(schedules.begin());
        }
    }
}


void GateControllerMultischedule::tick(IClock *clock, short kind){
    check_schedule();
    GateController::tick(clock, kind);
}

}
