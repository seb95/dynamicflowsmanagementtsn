//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TSN_APPLICATION_ETHERNET_ETHERNETAPPLICATION_H_
#define TSN_APPLICATION_ETHERNET_ETHERNETAPPLICATION_H_

#include <omnetpp.h>
#include <algorithm>

#include "inet/applications/ethernet/EtherTrafGen.h"

#include "inet/common/TimeTag_m.h"
#include "inet/linklayer/common/Ieee802SapTag_m.h"
#include "inet/linklayer/common/MacAddressTag_m.h"
#include "inet/common/ProtocolTag_m.h"
#include "inet/common/packet/chunk/ByteCountChunk.h"
#include "inet/common/Protocol.h"
#include "inet/common/Simsignals.h"
#include "inet/linklayer/ethernet/EtherFrame_m.h"
#include "inet/common/IProtocolRegistrationListener.h"
#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "inet/networklayer/common/L3AddressResolver.h"

#include "nesting/linklayer/vlan/EnhancedVlanTag_m.h"
#include "nesting/application/ethernet/VlanEtherTrafGenSched.h"
#include "nesting/linklayer/vlan/EnhancedVlanTag_m.h"

#include "../../common/schedule/SingleScheduleEtherTrafGen.h"
#include "../../common/components/Recorder.h"

using namespace omnetpp;
using namespace inet;
using namespace nesting;

namespace tsn {

class EthernetApplication : public EtherTrafGen {
public:
    static const Protocol* L2_PROTOCOL;
private:
    // Parameters from NED file
    cPar* vlanTagEnabled;
    cPar* pcp;
    cPar* dei;
    cPar* vid;
    int flowIndex;
    std::vector<SingleScheduleEtherTrafGen*> schedules;

    simtime_t computeDelay(double startTime, int i);
    void finish() override;
protected:
    virtual void initialize(int stage) override;
    virtual void sendBurstPackets(double sendInterval, double startTime);
    void handleMessageWhenUp(cMessage *msg) override;
    bool isGenerator() override;
    void receivePacket(Packet *msg) override;

};

} // namespace tsn

#endif /* TSN_APPLICATION_ETHERNET_ETHERNETAPPLICATION_H_ */
